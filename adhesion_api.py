from json import JSONDecodeError

from oauthlib.oauth2 import BackendApplicationClient, TokenExpiredError
from requests_oauthlib import OAuth2Session
from rest_framework import status
from rest_framework.exceptions import APIException

from billetterie.settings import ADHESION
import logging
logger = logging.getLogger('adhesion_api')
class AdhesionAPI():
    url = ADHESION['URL']
    client_id = ADHESION['CLIENT_ID']
    client_secret = ADHESION['CLIENT_SECRET']
    token = None
    client = None
    oauth = None
    def __init__(self, token=None):
        if token is not None:
            self.token=token
        self.refresh_token()
        super().__init__()

    @classmethod
    def refresh_token(self, needsRefresh=False):
        try:
            if (self.token is None and self.client is None) or needsRefresh:
                if needsRefresh: logger.info("Renouvellement du token car il est invalide")
                self.client = BackendApplicationClient(client_id=self.client_id)
                self.oauth = OAuth2Session(client=self.client)
                self.token = self.oauth.fetch_token(token_url=self.url + '/o/token/', client_id=self.client_id,
                                                    client_secret=self.client_secret)
                logger.info("Got token {} for external API".format(self.token['access_token']))
            else:
                logger.debug("Il y a dejà un Token : {} pour {}".format(self.token, self.client))
            #    r =self.client.refresh_token(self.url + '/o/token/') #ne faut-il pas le récupérer ??
            #    logger.debug(r.text)
        except:
            logger.exception("Impossible d'obtenir un token pour l'API externe Adhésion")

    def get(self, url, **kwargs):
        logger.info('api_va: GET {}'.format(url))
        import requests
        r = requests.get(url, headers={'Authorization': "Bearer "+self.token['access_token']}, **kwargs)
        logger.info("Réponse Adhésion ({}) : {}".format(str(r.status_code), r.text))
        if r.status_code == 401:
            logger.warning("Token expiré, on réessaie")
            self.refresh_token(needsRefresh=True) #visiblement y'a pas de refresh token avec une Application "client credentials"
            r = requests.get(url, headers={'Authorization': "Bearer " + self.token['access_token']}, **kwargs)
        print(r.status_code)
        print(r.text)
        #if r.status_code not in [status.HTTP_200_OK, status.HTTP_201_CREATED, status.HTTP_202_ACCEPTED]:
        #    raise APIException("Une erreur est survenue lors de la connexion à un serveur externe")
        return r
        #try:
        #    r = self.oauth.get(url, **kwargs, timeout=10)
        #    if r.status_code ==401:
        #        raise TokenExpiredError
        #    return r
        #except TokenExpiredError:
        #    self.client.refresh_token(url+'/o/token/')
        #    logger.info("Credentials expirés ? le token a été refresh")
        #        #self.__init__() sinon on peut aussi en redemander
        #except Exception:
        #    logger.exception("Impossible de se connecter à {} pour l'API externe".format(url))
    def get_user(self, user_id):
        r = self.get(self.url+'/oauth/user/{}/'.format(user_id))
        try:
            return r.json()
        except JSONDecodeError:
            logger.exception("Réponse inattendue : reçu ".format(r.text))

    def get_member(self, member_id):
        r = self.get(self.url+'/v1/members/{}/'.format(member_id))
        try:
            if r.status_code == 404: return None
            member = r.json()
            member['id']
            return member
        except KeyError: #keyerror si membre inexistant
            logger.exception(r.text)
            return None
        except AttributeError: #pas de réponse
            logger.exception(" ?")
            return None

    def try_member(self, member_id):
        if member_id is None: return False #pour éviter le flood
        r = self.get(self.url + '/v1/members/{}/'.format(member_id))
        try:
            if r.status_code == 404:
                return False
            elif r.status_code == 200:
                return True
            else:
                logger.debug("Erreur inconnue: "+str(r.status_code)+" "+r.text)
        except: #gaffe !
            logger.debug("Membre {} inexistant".format(member_id))
            return False

    def get_card(self, card):
        """
        Permet de savoir si une carte VA est valide
        :param card: le numéro de carte VA (avec cXXXX...)
        :return: l'ID de l'adhérent si la carte est valide
        """
        r = self.get(self.url+'/v1/cards/{}'.format(card))
        if r.status_code == 404:
            return False
        try:
            carte = r.json()
            if carte['valid_member'] and carte['activated']: #si la carte est active et qu'il a payé son adhésion
                print('carte {} valide'.format(card))
                #return True #la carte est désactivée quand quelqu'un en change (normalement)
                return carte['member']
            else: return None
        except KeyError:
            logger.exception(r.text)
            return None
        except AttributeError: #pas de réponse
            logger.exception(" ? ({})".format(card))
            return None

    def tester_carte_va(self, va_key, first_name, last_name, email, phone=None):
        member_id = self.get_card(va_key)
        if member_id is None: return False
        va_info = self.get_member(member_id)
        if va_info is None: return False
        try:
            va_info = {key: va_info[key] for key in ['first_name', 'last_name', 'email', 'phone']}
        except KeyError:
            return False
        if (first_name.lower() == va_info['first_name'].lower() and last_name.lower() == va_info[
            'last_name'].lower()) or email == va_info['email'] or (
        phone[:-9:-1] == va_info['phone'][:-9:-1] if phone else False):
            print('va & adhérent valides')
            return True