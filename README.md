# Gestion des inscription & dossiers pour le RAID INSA Lyon-Orange

### Infos utiles
Application web full Django, utilisant *templates* et *views* pour le *front-end*, contrairement à nos autres webapp avec Django REsT + Angular. La CI est un peu bullshit, en tout cas tant qu'on regarde l'output de ``runserver`` dans PyCharm

n'oubliez pas de renseigner la variable d'environnement ``DEBUG=True``, sinon vous n'aurez pas accès aux **fichiers statiques** (du coup un site tout moche !)

les fichiers mis en ligne par les participants finissent actuellement dans ``/uploads``

# Déploiement
Ça utilise docker-compose, avec 3 éléments:
* Django (bah oui) qui partage 2 volumes : ``uploads`` et ``staticfiles``
* Nginx qui sert _staticfiles_ et de manière protégée par Django _uploads_ et opère un proxy vers Django
* Mysql pour la base de données, avec un volume ``database`` pour sauvegarder les données

Pour que la communication avec la billetterie se fasse correctement, il faut donner à Dossieraid les informations concernant le paiement:
* id de l'évènement RAID
* id du produit "prix VA"
* id du produit "prix étudiant"
* id du produit "prix normal"
Il y a un modèle **BilleventCache** qui permet de noter ces informations. À voir plus tard si on fait en sorte de récupérer ces informations
avec une requête d'API ou de les stocker dans un fichier de config

Sur Billevent-API, il faut mettre les produits avec un nombre de "seats" égal à 1, sinon ça marche pas !
créer un volume sur /app/uploads pour savegarder les fichiers, et un autre sur /app/staticfiles pour les servir avec un serveur web (CDN ...)

```
DATABASE_URL= type://user:password@host:port/database
HOST=localhost,0.0.0.0,...
SSL=False
BILLEVENT_API_URL=https://some-billevent-api-server.com/
BILLEVENT_API_USERNAME=user
BILLEVENT_API_PASSWORD=password
BILLEVENT_RAID_EVENT= id de l'évènement RAID à gérer
BILLEVENT_PRIX_NORMAL= id du tarif normal
BILLEVENT_PRIX_EDU= id du tarif étudiant
BILLEVENT_PRIX_VA=  id du tarif carte VA (vérifiée par Dossieraid)
ADHESION_APP_ID=  pour que Dossieraid puisse vérifier les cartes VA via adhésion (ruby)
ADHESION_APP_SECRET= ....
FRONTEND_PERMANENCIER_URL= url entière de frontend-permanencier
DEFAULT_FROM_EMAIL= email par défaut pour mailgun
MAILGUN_API_KEY=
MAILGUN_DOMAIN=
ADMIN_EMAIL= adresse e-mail à laquelle envoyer les logs
```

### Technos utilisées:
 * *Django* (ben oui...)
 * *Materializecss* (oui j'ai totalement copié-collé mon site perso pour l'interface)
 * *Material-frontend* pour rendre les formulaire jolis automatiquement
 * ``django.contrib.auth`` le système d'authentification intégré à Django, c'est magique

#### Plus d'infos sur l'authentification
Dossieraid utilise l'authentification intégrée à Django, il suffit de l'intégrer dans les URLs et de créer 2-3 templates
le reste est à moitié automatique : il faut quand même décorer les fonction avec @login_required, et spécifier les permissions
si on veut les utiliser.
J'utilise 'gestion.change_participant' comme permission pour identifier les Orgas
au fait la syntaxe est ``app.action_modèle``

Il faudra créer des comptes ayant comme permission **Gestion | Participant | can change Participant**

#### Communication avec Billevent
le code qui marche est dans la branche **master** (7/5/2018). J'ai rajouté 2-3 endpoints

Comment ça marche ?
* Une fois que l'Orga valide un participant, un lien apparait sur son interface client.
* Quand il le clique, ça regarde s'il a déjà un Client sur Billevent en utilisant son adresse mail comme champ de recherche
* Si ça trouve un Client, dossieraid enregistre son ID
* s'il ne trouve pas de Client, dossieraid effectue une requête pour créee un Client, puis sauvegarde l'ID
* Un 2e lien apparaît alors sur l'interface du Participant
* quand il le clique, ça crée pour lui une invitation si il n'en a pas déjà (liées à son Client)
* à la création de l'Invitation, dossieraid crée aussi une InvitationGrant qui correspond au produit pour l'utilisateur (prix différents selon VA ou carte étudiante...)
* le mail est censé partir lors de la création de l'invitation, mais j'implémenterais un bouton pour le ré-envoyer

#### Status des participants
Les Participant ont un numéro ``status`` qui définit l'état de leur dossier. Ce statut est utilisé un peu partout dans le code,
et il peut être changé par les orgas.
