"""dossieraid URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from django.urls import include, path
from material.frontend import urls as frontend_urls
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import password_reset_confirm
from dossieraid.secure_dowload import  dl_file
urlpatterns = [
    url(r'', include(frontend_urls)),
    url(r'admin/', admin.site.urls),
    url(r'^accounts/password/reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
        password_reset_confirm, name='password_reset_confirm'),
    path('accounts/', include('django.contrib.auth.urls')),
    url(r'^orga/', include('orga.urls')),
    url(r'^', include('gestion.urls')),
    url(r'^media/(?P<path>.+)', dl_file), #délègue le téléchargement à Django-Sendfile
]
