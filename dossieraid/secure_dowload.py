from django.contrib.auth.decorators import permission_required
from sendfile import sendfile

@permission_required('gestion.change_participant') #restreint le téléchargement des fichiers aux Orgas
def dl_file(request, path): #sans utiliser Django pour le téléchargement (si on configure Nginx :) bien sûr)
    return sendfile(request, filename=path)