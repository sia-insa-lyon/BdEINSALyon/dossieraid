from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import TemplateView, ListView, View
from django.views.generic import CreateView, UpdateView, FormView, DetailView
from django.views.generic.edit import FormMixin, ModelFormMixin
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.views.generic.detail import SingleObjectMixin
from gestion.models import *
# Create your views here.
from orga.forms import *
from orga.talkapi import BilleventAPI
from django.contrib.auth.decorators import permission_required
from django.conf import settings

class InterfaceAdmin(LoginRequiredMixin, TemplateView):
    template_name = 'admin.html'


class ListeParticipants(PermissionRequiredMixin, ListView):
    permission_required = 'gestion.change_participant'
    model = Equipe
    template_name = 'liste_equipes.html'


class RajoutParticipant(PermissionRequiredMixin, CreateView):
    permission_required = 'gestion.change_participant'
    model = Participant
    form_class = ParticipantForm
    template_name = 'rajoutParticipant.html'

    def get_initial(self):
        equipe = get_object_or_404(Equipe, pk=self.kwargs.get('pk'))
        return {'equipe': equipe.id}

    success_url = '/orga/liste/'


class RajoutEquipe(PermissionRequiredMixin, CreateView):
    permission_required = 'gestion.change_participant'
    model = Equipe
    template_name = 'rajoutParticipant.html'
    fields = ['nom_equipe']
    success_url = '/orga/'


class EditeParticipant(PermissionRequiredMixin, UpdateView):
    permission_required = 'gestion.change_participant'
    model = Participant
    # fields = '__all__'
    form_class = ParticipantForm
    template_name = 'rajoutParticipant.html'
    #success_url = '/orga/liste'
    def get_success_url(self):
        next = self.request.GET.get('next')
        if next is not None: return next
        else: return '/orga/liste'


class TableauParticipants(PermissionRequiredMixin, ListView):
    permission_required = 'gestion.change_participant'
    model = Participant
    template_name = 'tableau_participants.html'
    context_object_name = 'participants'
    ordering = ['equipe']


class ValideParticipantView(PermissionRequiredMixin, UpdateView):
    permission_required = 'gestion.change_participant'
    model = Participant
    fields = ['paiement_prevente', 'resp_civile_valide', 'certif_medic_valide',
              'decharge_resp_valide','carte_edu_valide', 'carte_edu_prevente',
              'va_valide', 'resp_civile_prevente', 'certif_medic_prevente',
              'decharge_resp_prevente', 'status']
    success_url = '/orga/tableau/'
    template_name = 'valide_participant.html'

    def form_valid(self, form):
        data = form.save()
        if data.status == 1:
            return HttpResponseRedirect('/orga/billevent/'+str(data.id))
        else:
            return redirect('/orga/tableau/')

    def get_context_data(self, **context):
        participant = Participant.objects.get(id=self.kwargs['pk'])
        context[self.context_object_name] = self.object
        context['participant'] = participant
        return super().get_context_data()


@permission_required('gestion.change_participant')
def check_paiements(request, pk):
    p = Participant.objects.get(id=pk)
    if p.invitation_id is not None and p.status == 2:
        api = BilleventAPI()
        if api.has_payed(p.client_id):
            p.a_paye = True
            p.status = 3
            p.save()
            # return render(request, template_name='error.html', context={"error": "OUI"})
            return redirect('/orga/valide/' + str(p.id) + '/')
        else:
            return redirect('/orga/valide/' + str(p.id) + '/')
    else:
        return redirect('/orga/')
@permission_required('gestion.change_participant')
def check_all_paiements(request):
    participants = Participant.objects.filter(status=2, paiement_prevente=False, a_paye=False, client_id__isnull=False)
    api = BilleventAPI()
    retour = "Résultat:<br>"
    for p in participants: #itère tous les participants ayant commencé à payer sur la billetterie
        if api.has_payed(p.client_id):
            p.a_paye = True
            p.status = 3
            p.save()
            retour += "#" + str(p.id)+": OK<br>"
        else:
            retour += "#"+str(p.id)+": NON<br>"
    return HttpResponse(retour)


class ParticipantPreventeView(PermissionRequiredMixin, CreateView):
    permission_required = 'gestion.change_participant'
    model = Participant
    form_class = PreventeParticipantForm
    template_name = 'rajoutParticipant.html'
    success_url = settings.FRONTEND_PERMANENCIER_URL
    #success_url = '/orga/prevente'

    #def get_initial(self):
    #    super().get_initial()
    #    self.initial = {"paiement_prevente": True,
    #                    "resp_civile_prevente": True,
    #                    "certif_medic_prevente": True,
    #                    "decharge_resp_prevente": True,
    #                    "carte_edu_prevente": True}
    #    return self.initial

class PreventeListeView(TableauParticipants):
    queryset = Participant.objects.filter(user=None)  #les mec crés à la prévente on pas de user/mdp
    success_url = '/orga/prevente/'
    template_name = 'tableau_prevente.html'
    permission_required = 'gestion.change_participant'



@permission_required('gestion.change_participant')
def create_billevent(request, pk):
    p = Participant.objects.get(id=pk)
    api = BilleventAPI()
    if p.client_id is None:
        try:billeventClientID = api.client(p.email)
        except api.BilleventError as e: return HttpResponse(e.args) #ça c'est de la gestion d'erreurs !
        if billeventClientID is None:
            p.client_id = api.create_client(first_name=p.prenom, last_name=p.nom, username=p.user.username,
                                            email=p.email, phone=p.telephone_number, password=p.user.password)  # garde l'ID du nouveau Client
            if p.client_id is None: return HttpResponse("erreur du décodage de la requête<br>lors de la création du Client", status=500)
            p.save()  # on met ça dans la DB
        else:
            p.client_id = billeventClientID
            p.save()

    if p.invitation_id is None:
        # billeventInvitationID = api.invitation(client=p.client_id)
        billeventInvitationID = api.invitation(p.client_id)
        if billeventInvitationID is None:
            p.invitation_id = api.create_invitation(first_name=p.prenom, last_name=p.nom,
                                                    client=p.client_id, email=p.email,
                                                    prix=settings.BILLEVENT_PRIX[p.prix_type()])
            # ce bouzin prend demande le type de paiement (VA, Edu, normal) pour le client et le fait correspondre
            # à l'ID du produit corresponant dans Billevent
            p.status = 2  # passe le mec en status "paiement en cours"
            p.save()
            # return HttpResponse(p.invitation_id)
        else:
            p.invitation_id = billeventInvitationID
            p.save()
        if not p.invitation_link_sent:
            api.send_invitation(p.invitation_id) #pas fail-safe, mais facile à gérer
        return redirect('/orga/tableau/')
    else:
        return HttpResponse("Le participant a déjà une invitation sur la billetterie")

def sendlink(request, id):
    api = BilleventAPI()
    res = api.send_invitation(id)
    return HttpResponse(res)
