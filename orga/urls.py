from django.conf.urls import url
from orga.views import *
from django.views.decorators.http import require_POST
urlpatterns = [
    url(r'^$', InterfaceAdmin.as_view()),
    url(r'^liste/$', ListeParticipants.as_view()),
    url(r'^add/(?P<pk>[0-9]+)/', RajoutParticipant.as_view()),
    url(r'^edit/(?P<pk>[0-9]+)/$', EditeParticipant.as_view()),
    url(r'^equipe/$', RajoutEquipe.as_view()),
    url(r'^tableau/$', TableauParticipants.as_view()),
    url(r'^valide/(?P<pk>[0-9]+)/$', ValideParticipantView.as_view()),
    url(r'^payed/all$', check_all_paiements),
    url(r'^payed/(?P<pk>[0-9]+)/$', check_paiements),
    url(r'^prevente/add$', ParticipantPreventeView.as_view()),
    url(r'^prevente/$', PreventeListeView.as_view()),
    url(r'^billevent/(?P<pk>[0-9]+)$', create_billevent),
    url(r'^sendlink/(?P<id>[0-9]+)$', sendlink),

]