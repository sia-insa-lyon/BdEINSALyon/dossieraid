import requests
from django.conf import settings
from django.http import HttpResponse
from json import JSONDecodeError
import logging
from uuid import uuid1
logger = logging.getLogger("talkapi")
class RestAPI:
    def __init__(self):
        self.logger = logging.getLogger("talkapi")
        self.username = settings.BILLEVENT_API_USERNAME
        self.password = settings.BILLEVENT_API_PASSWORD
        self.apiUrl = settings.BILLEVENT_API_URL
        self.token = self.auth()
    class APIerror(Exception):
        pass

    def auth(self):
        try:
            res = requests.post(url=self.apiUrl + 'api/authenticate', data={
                "username": self.username,
                "password": self.password}
                                )
            try:
                json = res.json()
                return json['token']
            except JSONDecodeError:
                self.logger.error(res.text)
                raise self.APIerror("Erreur lors de l'authentification")
        except requests.RequestException:
            raise self.APIerror('Billevent injoignable')
            #return HttpResponse("Billevent est injoignable", status=500) en fait je peux afficher de Response ici

    def get(self, service, args=None):
        res = None
        try:
            res = requests.get(url=self.apiUrl + service, headers={"Authorization": "JWT " + self.token}, params=args)
            return res
        except requests.RequestException: #erreur "catch-all"
            self.logger.error(res.text)
            raise self.APIerror('Erreur de communication')

    def post(self, service, data):
        res = None
        try:
            res = requests.post(url=self.apiUrl + service, headers={"Authorization":
                                                                        "JWT " + self.token}, data=data)
            return res
        except requests.RequestException: #catch-all errors
            self.logger.error(res.text)
            raise self.APIerror("Erreur interne")

class BilleventAPI(RestAPI):
    def __init__(self):
        self.event_id = settings.BILLEVENT_RAID_EVENT
        self.prix_normal_id = settings.BILLEVENT_PRIX_NORMAL
        self.prix_edu_id = settings.BILLEVENT_PRIX_ETUDIANT
        self.prix_va_id = settings.BILLEVENT_PRIX_VA
        super().__init__()

    def client(self, email):
        res = self.get(service='api/client/?email=' + email)
        if res.text == '[]':
            return None
        else:
            try:
                return res.json()[0]['id']
            except JSONDecodeError:
                self.logger.error(res.text)
                raise self.BilleventError('Erreur de décodage JSON')

    def create_client(self, first_name, last_name, username, email, phone, password):
        data = {'first_name': first_name,
                'last_name': last_name,
                'email': email,
                'username': username,
                'password': password, #tkt, il est chiffré, je pourrais aussi bien envoyer un UUID
                'phone': phone}
        res = self.post(service='api/clientraid/', data=data)
        try:
            client = res.json()
            return client['id']
        except (JSONDecodeError, TypeError) as e:
            self.logger.error("RÉPONSE BILLEVENT: "+res.text)
            self.logger.error(e.args)
            raise RestAPI.APIerror("Erreur de décodage")


    def invitation(self, client):
        args = {'client': str(client),
                'event': str(self.event_id)}
        res = self.get(service='api/invitation/', args=args)
        if res.text == '[]':
            return None
        else:
            match = None #du coup si ça trouve que des invitations au Gala, il en créera quandd même
            for invitation in res.json():
                if invitation['event'] == self.event_id:
                    match = invitation['id']
            return match

    def create_invitation(self, first_name, last_name, client, email, prix):
        data = {
            "client": client,
            "first_name": first_name,
            "last_name": last_name,
            "email": email,
            "reason": "raid",
            "seats": 1,
            'event': self.event_id,
        }
        res = self.post(service='api/invitation/', data=data)
        a = res.text
        id = res.json()['id']
        self.grant_product(prix, id)
        return id


    class InvitationNotFound(Exception):
        pass

    def invitation_sent(self, invitation):
        res = self.get('api/invitation/' + str(invitation) + '/')
        try:
            query= res.json()
            if res.status_code == 200:
                if query['link_sent']:
                    return True
                else:
                    return False
            else:
                raise self.InvitationNotFound('Invitation inexistante')
        except:
            raise self.InvitationNotFound('Problème de communication, invitation inexistante')

    def send_invitation(self, invitation_id):
        if self.invitation_sent(invitation_id):
            res = self.get('api/invitation/'+str(invitation_id)+'/send_mail/')
            return res.text
        else: return False
    def grant_product(self, prix, invitation):
        data = {
                 "amount": 1,
                 "invitation": invitation,
                 "product": prix
                }
        res = self.post(service='api/grants/', data=data)
        if res.json() == data:
            return True
        else:
            return res.text

    def resend_mail(self, client):
        res = self.get('api/invitation/'+str(client)+'/send_mail/')
        if res.text == '"'+str(client)+'"':
            return True
        else: return False

    def has_payed(self, client):
        args = {"event": str(self.event_id),
                "client": str(client)}
        res = self.get(service='api/get_order/', args=args)
        if res.text == '[]': return False
        elif res.json()[0]['status'] == 7: return True
        else: return False

    class BilleventError(Exception): #connection réussie mais donnée incorrectes : versions incompatibles ...
        pass

from json import JSONDecodeError

from oauthlib.oauth2 import BackendApplicationClient, TokenExpiredError
from requests_oauthlib import OAuth2Session

from dossieraid.settings import ADHESION
class AdhesionAPI():
    url = ADHESION['URL']
    client_id = ADHESION['CLIENT_ID']
    client_secret = ADHESION['CLIENT_SECRET']
    token = None
    client = None
    oauth = None
    def __init__(self, token=None):
        if token is not None:
            self.token=token
        self.refresh_token()
        super().__init__()

    @classmethod
    def refresh_token(self, needsRefresh=False):
        try:
            if (self.token is None and self.client is None) or needsRefresh:
                if needsRefresh: logger.info("Renouvellement du token car il est invalide")
                self.client = BackendApplicationClient(client_id=self.client_id)
                self.oauth = OAuth2Session(client=self.client)
                self.token = self.oauth.fetch_token(token_url=self.url + '/o/token/', client_id=self.client_id,
                                                    client_secret=self.client_secret)
                logger.info("Got token {} for external API".format(self.token['access_token']))
            else:
                logger.debug("Il y a dejà un Token : {} pour {}".format(self.token, self.client))
            #    r =self.client.refresh_token(self.url + '/o/token/') #ne faut-il pas le récupérer ??
            #    logger.debug(r.text)
        except:
            logger.exception("Impossible d'obtenir un token pour l'API externe Adhésion")

    def get(self, url, **kwargs):
        logger.info('api_va: GET {}'.format(url))
        import requests
        r = requests.get(url, headers={'Authorization': "Bearer "+self.token['access_token']}, **kwargs)
        logger.info("Réponse Adhésion ({}) : {}".format(str(r.status_code), r.text))
        if r.status_code == 401:
            logger.warning("Token expiré, on réessaie")
            self.refresh_token(needsRefresh=True) #visiblement y'a pas de refresh token avec une Application "client credentials"
            r = requests.get(url, headers={'Authorization': "Bearer " + self.token['access_token']}, **kwargs)
        print(url+" "+str(r.status_code))
        print(r.text)
        return r
        #try:
        #    r = self.oauth.get(url, **kwargs, timeout=10)
        #    if r.status_code ==401:
        #        raise TokenExpiredError
        #    return r
        #except TokenExpiredError:
        #    self.client.refresh_token(url+'/o/token/')
        #    logger.info("Credentials expirés ? le token a été refresh")
        #        #self.__init__() sinon on peut aussi en redemander
        #except Exception:
        #    logger.exception("Impossible de se connecter à {} pour l'API externe".format(url))
    def get_user(self, user_id):
        r = self.get(self.url+'/oauth/user/{}/'.format(user_id))
        try:
            return r.json()
        except JSONDecodeError:
            logger.exception("Réponse inattendue : reçu ".format(r.text))

    def get_member(self, member_id):
        r = self.get(self.url+'/v1/members/{}/'.format(member_id))
        try:
            if r.status_code == 404: return None
            member = r.json()
            member['id']
            return member
        except KeyError: #keyerror si membre inexistant
            logger.exception(r.text)
            return None
        except AttributeError: #pas de réponse
            logger.exception(" ?")
            return None

    def try_member(self, member_id):
        if member_id is None: return False #pour éviter le flood
        r = self.get(self.url + '/v1/members/{}/'.format(member_id))
        try:
            if r.status_code == 404:
                return False
            elif r.status_code == 200:
                return True
            else:
                logger.debug("Erreur inconnue: "+str(r.status_code)+" "+r.text)
        except: #gaffe !
            logger.debug("Membre {} inexistant".format(member_id))
            return False

    def get_card(self, card):
        """
        Permet de savoir si une carte VA est valide
        :param card: le numéro de carte VA (avec cXXXX...)
        :return: l'ID de l'adhérent si la carte est valide
        """
        r = self.get(self.url+'/v1/cards/{}/'.format(card))
        if r.status_code == 404:
            return None
        try:
            carte = r.json()
            if carte['valid_member'] and carte['activated']: #si la carte est active et qu'il a payé son adhésion
                print('carte {} valide'.format(card))
                #return True #la carte est désactivée quand quelqu'un en change (normalement)
                return carte['member']
            else: return None
        except KeyError:
            logger.exception(r.text)
            return None
        except AttributeError: #pas de réponse
            logger.exception(" ? ({})".format(card))
            return None

    def tester_carte_va(self, va_key, first_name, last_name, email, phone=None):
        try:
            va_info= self.get_member_from_card(va_key)
            if va_info is None: return False, None
            try:
                va_info = {key: va_info[key] for key in ['first_name', 'last_name', 'email', 'phone']}
            except KeyError:
                return False, va_info
            if (first_name.lower() == va_info['first_name'].lower() and last_name.lower() == va_info[
                'last_name'].lower()) or email == va_info['email'] or (
                    phone[:-9:-1] == va_info['phone'][:-9:-1] if phone else False):
                print('va & adhérent valides')
                return True, va_info
        except AssertionError:
            if not settings.DEBUG:
                return False, None

    def get_member_from_card(self, code):
        r = self.get(self.url + '/v1/cards/{}/member/'.format(code))
        member = r.json()
        assert member['email'] is not None and member['email']
        assert member['first_name'] is not None and member['first_name']
        assert member['last_name'] is not None and member['last_name']
        return member