from django.forms import ModelForm
from gestion.models import Participant

class ParticipantForm(ModelForm):
    class Meta:
        model = Participant
        exclude = ['user', 'a_paye', 'client_id', 'invitation_id', 'invitation_link_sent']

class ValideParticipantForm(ModelForm):
    class Meta:
        model = Participant
        fields = ['a_paye', 'resp_civile_valide', 'certif_medic_valide',
                  'decharge_resp_valide',
                  'va_valide',
                  'carte_edu_valide']

class PreventeParticipantForm(ModelForm):
    class Meta:
        model = Participant
        exclude = ['user', 'a_paye', 'client_id', 'invitation_id', 'invitation_link_sent', 'is_leader', #'etudiant_insa',
                   'resp_civile', 'certif_medic', 'decharge_resp', 'carte_edu', 'numero_VA',
                   'resp_civile_valide', 'certif_medic_valide', 'carte_edu_valide', 'decharge_resp_valide']
