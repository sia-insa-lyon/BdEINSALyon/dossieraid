from django import forms
from gestion.models import *
# Fichier de définition des formulaires HTML

class ParticipantForm(forms.ModelForm): # ICI, ça fonctionne comme des ModelSerializer : ça produit un formulaire
    class Meta: # qui correspond au modèle; et ensuite on l'intègrera au rendu de la vue
        model = Participant
        exclude = ['a_paye', 'status', 'certif_medic_valide', 'resp_civile_valide', 'decharge_resp_valide',
                   'user', 'equipe', 'is_leader', 'resp_civile_prevente',
                   'certif_medic_prevente', 'decharge_resp_prevente',
                   'resp_civile', 'certif_medic', 'decharge_resp', 'va_valide', 'paiement_prevente',
                   'carte_edu_valide', 'carte_edu_prevente', 'carte_edu',
                   'client_id', 'invitation_link_sent', 'invitation_id']
    divers = forms.CharField(widget=forms.Textarea, required=False, label='Informations additionnelles diverses')
class ParticipantLeaderForm(ParticipantForm):
    nouvelle_equipe = forms.CharField(max_length=255, label='Nom de la nouvelle équipe')

class ParticipantEditForm(forms.ModelForm):
    class Meta:
        model = Participant
        exclude = ['a_paye', 'status', 'certif_medic_valide', 'resp_civile_valide', 'decharge_resp_valide',
                   'user', 'equipe', 'is_leader', 'resp_civile_prevente',
                   'certif_medic_prevente', 'decharge_resp_prevente', 'va_valide', 'paiement_prevente',
                   'carte_edu_valide', 'carte_edu_prevente', 'email', 'client_id', 'invitation_link_sent',
                   'invitation_id']

class InvitationForm(forms.ModelForm):
    class Meta:
        model = Invitation
        fields = ['email']
