from django.conf.urls import url
from gestion import views
#Urls du l'appli unique "gestion"; enfin sauf si on sectionne le projet

urlpatterns = [
    url(r'^$', views.espace_client),
    url(r'^stats', views.index),
    url(r'^participant/add/(?P<spk>.+)$', views.rajout_participant, name='rajout-participant'),
    url(r'^leader/add', views.rajout_participant_equipe),
    url(r'^accounts/add\?next=(?P<url>)', views.ajout_user),
    url(r'^accounts/add/', views.ajout_user),
    url(r'^profile/edit', views.edit_dossier),
#    url(r'^profile/create', views.creerDossier),
    url(r'^action/quitter/$', views.desinscription),
    url(r'^participant/inviter', views.rajout_invitation),
    url(r'^participant/annuler/(?P<pk>[0-9]+)/$', views.annuler_invitation),
    url(r'^check_va/', views.check_va, name='api_va'),
    url(r'^client', views.client_billevent),
    url(r'^invitation', views.invitation_billevent),
    url(r'^resend', views.resend_mail_billetterie),
]
