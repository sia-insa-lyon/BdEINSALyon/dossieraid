from django.core.mail import EmailMultiAlternatives
#from django.shortcuts import render
from django.template.loader import get_template

class InvitationEmail(EmailMultiAlternatives):
    def __init__(self, lien, equipe, emetteur,**kwargs):
        self.lien = lien
        self.equipe = equipe
        self.emetteur = emetteur
        super().__init__("Invitation au RAID", self.generate_text(), **kwargs)
        self.attach_alternative(self.generate_html(), 'text/html')

    def get_context(self):
        return {
            'lien':self.lien,
            'equipe':self.equipe,
            'emetteur':self.emetteur
        }
    def generate_html(self):
        return get_template('invitation-email.html').render(self.get_context())
    def generate_text(self):
        return get_template('invitation-email.txt').render(self.get_context())

class PaiementEmail(EmailMultiAlternatives):
    def __init__(self, lien, code, montant, **kwargs):
        self.lien = lien
        self.code = code,
        self.montant = montant
        super().__init__("Paiement des frais du RAID", self.generate_text(), **kwargs)
        self.attach_alternative(self.generate_html(), 'text/html')

    def get_context(self):
        return {
            'lien':self.lien,
            'code':self.code,
            'montant':self.montant,
        }
    def generate_html(self):
        return get_template('paiement-email.html').render(self.get_context())
    def generate_text(self):
        return get_template('paiement-email.txt').render(self.get_context())