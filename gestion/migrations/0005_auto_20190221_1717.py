# Generated by Django 2.0.2 on 2019-02-21 16:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestion', '0004_auto_20180307_2238'),
    ]

    operations = [
        migrations.DeleteModel(
            name='BilleventCache',
        ),
        migrations.AlterField(
            model_name='participant',
            name='invitation_link_sent',
            field=models.BooleanField(default=False, verbose_name='Lien de paiement envoyé'),
        ),
    ]
