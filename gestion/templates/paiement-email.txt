Paiement des frais du RAID

Bonjour,
Votre dossier d'inscription a été validé, ainsi que celui de votre équipe.
Veuillez coller dans la barre d'addresse de votre navigateur le lien ci-dessous.
{{ lien }}
Vous serez redirigés vers la billetterie pour payer {{ montant }} €.
Vous devrez ensuite entrer ce code : {{ code }}
Bonne continuation !
L'Équipe du RAID
