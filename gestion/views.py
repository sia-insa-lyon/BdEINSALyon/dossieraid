from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from gestion.models import Equipe, Participant
from gestion.forms import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from gestion.models import *
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import UpdateView
from gestion.email import *
from django.views import View
from datetime import date

def erreur(request, message):
    return render(request, template_name='error.html',
           context={'error': message})
# Create your views here.

def index(request):
    equipes_valides = 0
    for equipe in Equipe.objects.filter(status=3):
        if Participant.objects.filter(equipe=equipe, status=3, a_paye=True).count() == 4:
            equipes_valides += 1
    context = {
        'nombre_equipes': Equipe.objects.all().count(),
        'nombre_participants': Participant.objects.all().count(),
        'equipes_valides': equipes_valides,
    }
    return render(request, template_name='index.html', context=context)


def ajout_user(request):
    form = UserCreationForm
    if request.method == 'POST':
        donnees = UserCreationForm(request.POST)
        if donnees.is_valid():
            donnees.save()
            username = donnees.cleaned_data.get('username')
            raw_password = donnees.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect(request.GET.get('next') or '/leader/add')
        else:
            return render(request, template_name='rajoutUser.html', context={'form': donnees})
            # ça affichera les erreurs à côté des champs incriminés
    else:
        if request.GET.get('next') is not None:
            return render(request, template_name='rajoutUser.html', context={'form': form})
        else:
            return render(request, template_name='leader_add.html', context={'form': form})


@login_required(login_url='/accounts/add')
def rajout_participant(request, spk):
    signer = Signer()
    pk = signer.unsign(spk)
    try:
        participant = Participant.objects.get(user=request.user)
        return render(request, template_name='error.html',
                      context={'error': 'Vous ne pouvez pas être dans deux équipes à la fois'})
    except Participant.DoesNotExist:
        try:
            invitation = Invitation.objects.get(pk=pk)
            real_equipe = invitation.equipe
            if invitation.utilisable:
                if request.method == 'POST':
                    donnees = ParticipantForm(request.POST, request.FILES)  # sinon Django ne trouve pas les fichers
                    if donnees.is_valid():
                        final = donnees.save(commit=False)
                        final.equipe = real_equipe
                        final.user = request.user
                        request.user.email = final.email
                        request.user.save()
                        if final.annee == 0 and final.departement == 'EXT':
                            final.etudiant_insa = False
                        if final.anniv > date(year=2000, day=6, month=4):
                            return render(request, template_name='error.html', context={'error':"Vous devez être majeur au moment du RAID"})
                        final.save()
                        invitation.utilisable = False
                        invitation.save()
                        return redirect('/')
                    else:
                        return render(request, template_name='rajoutParticipant.html', context={'form': donnees,
                                                                                                'equipe': real_equipe,
                                                                                                'header': "Création d'un nouveau dossier"
                                                                                                })
                else:
                    form = ParticipantForm(initial={'email': invitation.email})
                    return render(request, template_name='rajoutParticipant.html', context={'form': form,
                                                                                            'equipe': real_equipe,
                                                                                            'header': "Création d'un nouveau dossier",
                                                                                            "email": invitation.email
                                                                                            })
            else:
                return render(request, template_name='error.html',
                              context={'error': 'Cette invitation a été désactivée'})
        except Invitation.DoesNotExist:
            return render(request, template_name='error.html',
                          context={'error': "Cette invitation n'existe pas"})


@login_required()
def rajout_participant_equipe(request):
    try:
        participant = Participant.objects.get(user=request.user)
        return render(request, template_name='error.html',
                      context={'error': 'Vous ne pouvez pas être dans deux équipes à la fois'})
    except Participant.DoesNotExist:
        if request.method == 'POST':
            donnees = ParticipantLeaderForm(request.POST, request.FILES)
            if donnees.is_valid():
                nouvelle_equipe = donnees.cleaned_data['nouvelle_equipe']
                final = donnees.save(commit=False)
                final.user = request.user
                if Equipe.objects.filter(nom_equipe=nouvelle_equipe).count() >0:
                    return erreur(request, "Une équipe avec ce nom existe déjà")
                Equipe(nom_equipe=nouvelle_equipe).save()
                final.equipe = Equipe.objects.get(nom_equipe=nouvelle_equipe)
                final.is_leader = True
                if final.annee == 0 and final.departement == 'EXT':
                    final.etudiant_insa = False
                request.user.email = final.email
                request.user.save()
                if final.anniv > date(year=2000, day=6, month=4):
                    return  erreur(request, "Vous devez être majeur au moment du RAID")
                final.save()
                return redirect('/')
            else:
                return render(request, template_name='rajoutParticipant.html', context={'form': donnees,
                                                                                        'header': "Création d'un dossier et d'une équipe"})
        else:
            form = ParticipantLeaderForm
            return render(request, template_name='rajoutParticipant.html', context={'form': form,
                                                                                    'header': "Création d'un dossier et d'une équipe"})


@login_required()
def edit_dossier(request):
    try:
        participant = Participant.objects.get(user=request.user)
        # participant = get_object_or_404(Participant, user=request.user)
        form = ParticipantEditForm(request.POST or None, request.FILES or None, instance=participant)
        if form.is_valid():
            form.save()
            return redirect('/')
        else:
            return render(request, template_name='rajoutParticipant.html',
                          context={'form': form, 'header': "Modification de vos informations",
                                   "upload":"true"})
        # return render(request, template_name='rajoutParticipant.html', context={'form': form})
    except Participant.DoesNotExist:
        return render(request, template_name='error.html',
                      context={'error': 'Veuillez créer un profil avant de le modifier !'})


def espace_client(request):
    if str(request.user) != "AnonymousUser":
        try:
            participant = Participant.objects.get(user=request.user)
            typeUser = 'participant'  # quelqu'un qui a commencé à remplir son dossier
            return render(request, template_name="espace_client.html",
                          context={'type': typeUser,
                                   'status_texte': PARTICIPANT_STATUS[participant.status][1],
                                   'status_type': PARTICIPANT_STATUS[participant.status][0],
                                   'progression': participant.progress_bar, # slider en %
                                   'equipe': participant.equipe,
                                   'certif_medic_valide': participant.certif_medic_valide,
                                   'resp_civile_valide': participant.resp_civile_valide,
                                   'decharge_resp_valide': participant.decharge_resp_valide,
                                   'carte_edu_valide': participant.carte_edu_valide,
                                   'is_leader': str(participant.is_leader),
                                   'invitations': participant.invitations.filter(utilisable=True),
                                   'participants': participant.equipe.participants.all(),
                                   'va_valide': participant.va_valide,
                                   'numero_VA': participant.numero_VA,
                                   'client_id':participant.client_id,   #client dans la billetterie
                                   'invitation_id':participant.invitation_id, #liens pour payer
                                   'email': participant.email,
                                   })
        except Participant.DoesNotExist:
            typeUser = 'utilisateur'  # un mec qui s'est juste créé pseudo/mdp
            return render(request, template_name="espace_client.html", context={'type': typeUser})
    else:
        typeUser = 'visiteur'  # un nouveau venu
        return render(request, template_name="espace_client.html", context={'type': typeUser})


@login_required()
def desinscription(request):
    try:
        participant = Participant.objects.get(user=request.user)
        participant.status = 4
        participant.save()
        return render(request, template_name='error.html', context={'error': "Vous avez été désinscrit :("})
    except Participant.DoesNotExist:
        return render(request, template_name='error.html',
                      context={'error': "Vous n'aviez pas fait de dossier, vous ne pouvez donc pas être désinscrit"})


@login_required()
def rajout_invitation(request):
    if request.method == 'POST':
        donnees = InvitationForm(request.POST)
        participant = Participant.objects.get(user=request.user)
        total = participant.invitations.filter(utilisable=True).count() + participant.equipe.nombre_participants
        if donnees.is_valid():
            if total < 4:
                invitation = donnees.save(commit=False)
                invitation.emetteur = participant
                invitation.equipe = Participant.objects.get(user=request.user).equipe
                try:
                    invitation.save()
                    #return HttpResponse(invitation.lien_email(request=request)+'<br>'+invitation.lien)
                    #return HttpResponse(url+"<br>"+lien)
                    invitation.envoyer_invitation(request)
                except Exception as e:
                    raise e
                return redirect('/')
            else:
                return render(request, template_name='error.html',
                              context={'error': 'Vous ne pouvez pas avoir plus de personnes dans cette équipe. Actuellement :'+str(total)})
        else:
            return render(request, template_name='inviterParticipant.html', context={'form': donnees})
    else:
        return render(request, template_name='inviterParticipant.html',
                      context={'form': InvitationForm, 'header': "Envoyer un lien"})


@login_required()
def annuler_invitation(request, pk):
    invitation = Invitation.objects.get(pk=pk)
    invitation.utilisable = False
    invitation.save()
    return redirect('/')


class EditeParticipant(LoginRequiredMixin, UpdateView):
    model = Participant
    form_class = ParticipantEditForm
    template_name = 'rajoutParticipant.html'
    success_url = '/'
    def get_context_data(self, **kwargs):
        kwargs['upload'] = 'oui'
        return super().get_context_data()

from orga.talkapi import BilleventAPI, AdhesionAPI
from django.conf import settings

@login_required()
def client_billevent(request):
    p = Participant.objects.get(user=request.user)
    if p.client_id is None:
        api = BilleventAPI()
        billeventClientID = api.client(p.email)
        if billeventClientID is None:
            p.client_id = api.create_client(first_name=p.prenom, last_name=p.nom, username=p.user.username, email=p.email) #garde l'ID du nouveau Client
            p.save()    #on met ça dans la DB
            #return HttpResponse(json.dump(client)) #dev
            return redirect('/')
        else:
            p.client_id = billeventClientID #il en avait déjà un
            p.save()
            return redirect('/')
            #return HttpResponse(billeventClientID)
    else:
        return redirect('/')
        #return HttpResponse(p.client_id)


@login_required()
def invitation_billevent(request):
    p = Participant.objects.get(user=request.user)
    if p.status >=1:
        if p.invitation_id is None:
            api = BilleventAPI()
            #billeventInvitationID = api.invitation(client=p.client_id)
            billeventInvitationID = api.invitation(p.client_id)
            if billeventInvitationID is None:
                p.invitation_id = api.create_invitation(first_name=p.prenom, last_name=p.nom,
                                                      client=p.client_id, email=p.email,
                                                        prix=settings.BILLEVENT_PRIX[p.prix_type()])
                                                        #prix=BilleventCache.objects.get(type=BilleventCache.TYPES[p.prix_type()]).key)
                #ce bouzin prend demande le type de paiement (VA, Edu, normal) pour le client et le fait correspondre
                # à l'ID du produit corresponant dans Billevent
                p.status = 2 #passe le mec en status "paiement en cours"
                p.save()
                return redirect('/')
                #return HttpResponse(p.invitation_id)
            else:
                p.invitation_id = api.invitation(p.client_id)
                p.save()
                return redirect('/')
        else: return HttpResponse('Vous avez déjà une invitation. Regardez vos mails')
    else:
        return HttpResponse("Votre dossier n'a pas été validé par les Orgas")
#{ #reponse type de api/invitation
#    "first_name": "dfg",
#    "last_name": "dfg",
#    "email": "dfg@dfg.ty",
#    "event": 1,
#    "seats": 999
#}
@login_required()
def resend_mail_billetterie(request):
    p = Participant.objects.get(user=request.user)
    if p.invitation_id is not None:
        api = BilleventAPI()
        if api.resend_mail(p.client_id):
            return redirect('/')
        else: return render(request, template_name='error.html', context={"error":'Erreur interne, contactez le BdE'})
    else:
        return render(request, template_name='error.html', context={"error": "Erreur: vous n'avez pas initié le paiement"})

@login_required
def check_va(request):
    participant = Participant.objects.get(user=request.user)
    va_key = participant.numero_VA
    if participant.va_valide:
        return render(request, template_name="error.html", context={"error": "Votre carte VA \"{}\" a déjà été validée !".format(va_key)})
    api = AdhesionAPI()
    valide, va_info = api.tester_carte_va(va_key, participant.prenom, participant.nom, participant.email, participant.telephone_number)
    if valide:
        participant.va_valide = True
        participant.save()
        return redirect('/')
    else:
        error = "Le(s) champ(s) suivant(s) de vos informations VA ({}) ne correspond(ent) pas : ".format(va_key)
        if participant.prenom.lower() != va_info['first_name'].lower(): error += "Prénom : {}, ".format(va_info['first_name'])
        if participant.nom.lower() != va_info['last_name'].lower(): error += "Nom : {}, ".format(va_info['last_name'])
        if participant.email != va_info['email']: error += "Adresse électronique : {}, ".format(va_info['email'])
        if participant.telephone_number != va_info['phone']: error += "Numéro de téléphone : {}".format(va_info['phone'])
        error+= "contactez les organisateurs si vous pensez que votre carte VA est valide"
        return render(request, template_name="error.html", context={"error": error})