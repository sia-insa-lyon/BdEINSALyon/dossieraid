from django.contrib import admin
from django.http import HttpResponse

from gestion.models import *
@admin.register(Equipe)
class BasicAdmin(admin.ModelAdmin):
    pass
def export_csv(modeladmin, request, queryset):
    import csv
    from django.utils.encoding import smart_str
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=participants.csv'
    writer = csv.writer(response, csv.excel)
    response.write(u'\ufeff'.encode('utf8'))  # BOM (optional...Excel needs it to open UTF-8 file properly)
    writer.writerow([
        smart_str(u"n°"),
        smart_str(u'Prénom'),
        smart_str(u"Nom"),
        smart_str(u"Équipe"),
        smart_str(u"Sexe"),
        smart_str(u"Nom d'utilisateur"),
        smart_str(u"Prévente ?"),
        smart_str(u"E-mail"),
        smart_str(u"Numéro de téléphone"),
        smart_str(u"Date de naissance"),
        smart_str(u"Taille(cm)"),
        smart_str(u"Taille de T-Shirt"),
        smart_str(u"Études"),
        smart_str(u"PAYÉ ?"),
        smart_str(u"Tarif"),
        smart_str(u"Att. de \nResp. Civile"),
        smart_str(u"Certic. Médic."),
        smart_str(u"Décharge de resp."),
        smart_str("Preuve de \ncarte étudiante"),
        smart_str(u"Status"),
        smart_str(u"Remarques etc..."),

    ])
    for obj in queryset:
        writer.writerow([
            smart_str(obj.pk),
            smart_str(obj.prenom),
            smart_str(obj.nom),
            smart_str(obj.equipe),
            smart_str(obj.sexe_verbose),
            smart_str(obj.user),
            smart_str(obj.prevente),
            smart_str(obj.email),
            smart_str(obj.telephone_number),
            smart_str(obj.anniv),
            smart_str(obj.taille),
            smart_str(obj.taille_Tshirt),
            smart_str(obj.niveau_etudes),
            smart_str(obj.paiement_valide),
            smart_str(obj.va_ou_edu_valide),
            smart_str(obj.resp_civile_exist),
            smart_str(obj.certif_medic_exist),
            smart_str(obj.decharge_resp_exist),
            smart_str(obj.carte_edu_exist),
            smart_str(obj.status_verbose),
            smart_str(obj.divers),
        ])
    return response


export_csv.short_description = u"Exporter ces lignes au format CSV"
@admin.register(Participant)
class ParticipantAdmin(admin.ModelAdmin):
       list_filter = ['equipe', 'status']
       actions = [export_csv]
       ordering = ['equipe', 'status']
       list_display = ['id','prenom','nom','email','equipe','status','prevente','va_ou_edu_valide']

def renvoyer_mail(modeladmin, request, queryset):
    for invitation in queryset.filter(utilisable=True):
        invitation.envoyer_invitation(request)

@admin.register(Invitation)
class InvitationAdmin(admin.ModelAdmin):
    list_filter = ['utilisable']
    list_display = ['id','email','equipe','utilisable']
    actions = [renvoyer_mail]
#class ExportCSV(admin.AdminSite):
#    actions = [export_csv]
#
#admin.site.register(model_or_iterable=Participant, admin_class=ExportCSV)
#
# Register your models here.
