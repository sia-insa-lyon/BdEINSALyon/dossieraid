from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth.models import User
from django.urls import reverse
from gestion.email import InvitationEmail
from gestion.validate_pdf import validate_pdf
from django.core.signing import Signer
# Create your models here.

EQUIPE_STATUS = (
    (0, 'Non complète'),
    (1, 'Pas validé'),
    (2, 'Annulée'),
    (3, 'Validée/Inscrite'),
    (4, 'Refusée')
)


class Equipe(models.Model):
    class Meta:
        verbose_name = 'Équipe'

    nom_equipe = models.CharField(max_length=255, verbose_name='Nom de l\'équipe', unique=True)
    status = models.IntegerField(choices=EQUIPE_STATUS, verbose_name='Status de l\'équipe', default=0)

    @property
    def nombre_participants(self):
        return self.participants.filter(status__lt=2).count() #compte uniquement les participants non refusés et non  désistés

    @property
    def est_valide(self):
        if self.nombre_participants == 4 and self.status == 3:
            return True
        else:
            return False

    def __str__(self):
        return ' # ' + str(self.id) + ' ' + self.nom_equipe + ', ' + str(self.nombre_participants) + ' membres'

    @property
    def for_email(self):
        return str(self.nom_equipe)

PARTICIPANT_STATUS = (
    (0, 'Pas validé'),
    (1, 'Valide'),
    (2, 'Paiement en cours'),
    (3, 'Inscrit, paiement effectué'),
    (4, 'Désisté'),  # annulation pour cause de maladie
    (5, 'Refusé')    #annulé par l'Orga
)
SEXE = (
    (0, 'Unspecified'),
    (1, 'Homme'),
    (2, 'Femme'),
)

TAILLE_TSHIRT = (
    ('S','S'),
    ('M','M'),
    ('L','L'),
    ('XL','XL')
)

class Participant(models.Model):
    class Meta:
        verbose_name = 'Participant'

    prenom = models.CharField(max_length=255, verbose_name='Prénom')
    nom = models.CharField(max_length=255, verbose_name='Nom')
    user = models.OneToOneField(User, on_delete=models.PROTECT, null=True)
    telephone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                     message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    telephone_number = models.CharField(validators=[telephone_regex], max_length=17,
                                        verbose_name="Numéro de téléphone")  # validators should be a list
    anniv = models.DateField(verbose_name='Date de naissance')
    sexe = models.IntegerField(choices=SEXE, default=0)
    taille = models.IntegerField(verbose_name='Taille (cm)')
    taille_Tshirt = models.CharField(verbose_name='Taille de T-shirt', max_length=255, choices=TAILLE_TSHIRT)
    email = models.EmailField(max_length=255)
    status = models.IntegerField(choices=PARTICIPANT_STATUS, verbose_name='Status participant', default=0)
    a_paye = models.BooleanField(verbose_name='Paiement effectué', default=False)
    paiement_prevente = models.BooleanField(verbose_name="A payé lors d'une prévente", default=False)
    equipe = models.ForeignKey(to=Equipe, related_name='participants', on_delete=models.CASCADE, verbose_name='Équipe',
                               null=True)
    is_leader = models.BooleanField(verbose_name='A créé son équipe', default=False)
    # validation des documents
    def upload_participant_resp_civile(self, filename):
        return 'uploads/part_{0}/resp_civile/{1}'.format(self.id, filename)
    def upload_participant_certif_medic(self, filename):
        return 'uploads/part_{0}/certif_medic/{1}'.format(self.id, filename)
    def upload_participant_decharge_resp(self, filename):
        return 'uploads/part_{0}/decharge_resp/{1}'.format(self.id, filename)
    def upload_participant_carte_edu(self, filename):
        return 'uploads/part_{0}/carte_edu/{1}'.format(self.id, filename)
    resp_civile_valide = models.BooleanField(verbose_name='Attestation de reponsabilité civile validée', default=False)
    resp_civile_prevente = models.BooleanField(verbose_name='Attestation de reponsabilité civile donnée à la prévente', default=False)
    resp_civile = models.FileField(upload_to=upload_participant_resp_civile, verbose_name='Attestation de responsabilité civile',
                                   blank=True, validators=[validate_pdf])
    certif_medic_valide = models.BooleanField(verbose_name='Certificat médical validé', default=False)
    certif_medic_prevente = models.BooleanField(verbose_name='Certificat médical donnée à la prévente', default=False)
    certif_medic = models.FileField(upload_to=upload_participant_certif_medic,
                                    verbose_name='Certificat médical de non contre-indication à la pratique du raid multisport en compétition.',
                                    blank=True, validators=[validate_pdf])
    decharge_resp_valide = models.BooleanField(verbose_name='Décharge de responsabilité  signée', default=False)
    decharge_resp_prevente = models.BooleanField(verbose_name='Décharge de responsabilité donnée à la prévente', default=False)
    decharge_resp = models.FileField(upload_to=upload_participant_decharge_resp,
                                     verbose_name='Décharge (autorisation photo, nage, accord avec le règlement)',
                                     blank=True, validators=[validate_pdf])
    carte_edu_valide = models.BooleanField(verbose_name='Carte Étudiante validée(pas forcément INSA)', default=False)
    carte_edu_prevente = models.BooleanField(verbose_name='Preuve de carte étudiante donnée à la prévnte', default=False)
    carte_edu = models.FileField(upload_to=upload_participant_carte_edu, verbose_name='Preuve de carte étudiante',
                                 blank=True, validators=[validate_pdf])

    #etudiant_valide = models.BooleanField(verbose_name='Status étudiant validé', default=False)
    #etudiant_insa = models.BooleanField(verbose_name='Étudie (vraiment) à l\'INSA', default=False)
    DEPARTEMENTS = (('PC', 'Premier Cycle'), ('IF', 'Informatique'), ('TC', 'Télécom'),
                                            ('GM', 'Génie Mécanique'), ('SGM', 'Science et Génie des Matériaux'),
                                            ('GI', 'Génie Industriel'), ('BS', 'Biosciences'),
                                            ('GE', 'Génie Éléctrique'),
                                            ('GCU', 'Génie Civil et Urbanisme'),
                                            ('GEN', 'Génie Énergétique et Environnement'),
                                            ('EXT', "N'est pas à l'INSA"))
    departement = models.CharField(choices=DEPARTEMENTS, max_length=3,
                                   verbose_name='Département', default='EXT')
    ANNEES = [(0, 'Extérieur'), (1, '1A'), (2, '2A'), (3, '3A'), (4, '4A'), (5, '5A')]
    annee = models.IntegerField(choices=ANNEES,
                                verbose_name='Année d\'études', default=0)
    numero_VA = models.CharField(verbose_name='Numéro de carte VA si applicable', null=True, blank=True, max_length=255)
    va_valide = models.BooleanField(verbose_name='Carte VA valide', default=False)
    divers = models.CharField(verbose_name="Remarques, régime alimentaire parituculier ..", max_length=2000, blank=True)
    client_id = models.IntegerField(verbose_name="ID de client sur Billevent", null=True, blank=True)
    invitation_id = models.IntegerField(verbose_name="ID du lien d'invitation sur billevent", null=True, blank=True)
    invitation_link_sent = models.BooleanField(verbose_name="Lien de paiement envoyé", default=False)

    def __str__(self):
        return '#' + str(self.id) + ' ' + self.nom + ' ' + self.prenom + ', équipe (' + str(
            self.equipe.id) + ') ' + self.equipe.nom_equipe

    @property
    def nom_prenom(self):
        return str(self.prenom) + ' ' + str(self.nom)

    @property
    def status_verbose(self):
        return PARTICIPANT_STATUS[self.status][1]
    @property
    def va_ou_edu_valide(self):
        if self.va_valide: return 'VA'
        elif self.carte_edu_valide: return 'Edu'
        else: return 'Normal'
    @property
    def niveau_etudes(self):
        if self.annee !=0 and self.departement != 'EXT':
            return self.ANNEES[self.annee][1]+' '+ self.departement
        else:
            return 'Ext.'
    @property
    def sexe_verbose(self):
        return SEXE[self.sexe][1]

    @property
    def resp_civile_exist(self):
        if self.resp_civile:
            if self.resp_civile_valide: return 'Valide'
            else: return 'En ligne'
        elif self.resp_civile_prevente:
            if self.resp_civile_valide: return 'Valide'
            else: return 'Prévente'
        else: return ''

    @property
    def certif_medic_exist(self):
        if self.certif_medic:
            if self.certif_medic_valide: return 'Valide'
            else: return 'En ligne'
        elif self.certif_medic_prevente:
            if self.certif_medic_valide: return 'Valide'
            else: return 'Prévente'
        else: return ''

    @property
    def decharge_resp_exist(self):
        if self.decharge_resp:
            if self.decharge_resp_valide: return 'Valide'
            else: return 'En ligne'
        elif self.decharge_resp_prevente:
            if self.decharge_resp_valide: return 'Valide'
            else: return 'Prévente'
        else: return ''

    @property
    def carte_edu_exist(self):
        if self.carte_edu:
            if self.carte_edu_valide: return 'Valide'
            else: return 'En ligne'
        elif self.carte_edu_prevente:
            if self.carte_edu_valide: return 'Valide'
            else: return 'Prévente'
        else: return ''
    @property
    def paiement_valide(self):
        if self.a_paye : return 'Billetterie'
        elif self.paiement_prevente: return 'Prévente'
        else: return 'Non'
    def prix_type(self):
        if self.va_ou_edu_valide == 'VA': return 'prixVA'
        elif self.va_ou_edu_valide == 'Edu': return 'prixEdu'
        else: return 'prixNormal'
    @property
    def prevente(self):
        if self.user is None: return True
        else: return False

    def progress_bar(self):
        value = 0
        value +=int(self.carte_edu_valide)
        value +=int(self.va_valide)
        value +=int(self.resp_civile_valide)
        value +=int(self.certif_medic_valide)
        value +=int(self.decharge_resp_valide)
        if self.a_paye: return 90
        elif self.status == 2: return 70
        elif self.status == 3: return 100
        return value*10


class Invitation(models.Model):
    class Meta:
        verbose_name = 'Invitation'

    emetteur = models.ForeignKey(Participant, on_delete=models.CASCADE, related_name='invitations', null=True)
    equipe = models.ForeignKey(Equipe, on_delete=models.CASCADE, related_name="invitations", null=True)
    email = models.EmailField(verbose_name="Adresse de réception de l'invitation")
    # lien = models.URLField(verbose_name="Lien d'invitation")
    utilisable = models.BooleanField(verbose_name='Peut être utilisée', default=True)

    def lien_email(self, request):
        return request.build_absolute_uri(self.lien)
        #return request.build_absolute_uri(reverse('rajout-participant', args=[signer.sign(str(self.id))]))

    @property
    def lien(self):
        return reverse('rajout-participant', args=[Signer().sign(self.id)])

    @property
    def utilisee(self):
        return Participant.objects.filter(email=self.email).count()

    def __str__(self):
        return '#' + str(self.id) + ' ' + self.email

    def envoyer_invitation(self, request):
        mail = InvitationEmail(to=[self.email], lien=self.lien_email(request), equipe=self.equipe.for_email,
                               emetteur=self.emetteur.nom_prenom)
        mail.send()

